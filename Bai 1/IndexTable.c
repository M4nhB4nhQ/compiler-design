#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct {
  int *array;
  size_t used;
  size_t size;
} Array;

void initArray(Array *a, size_t initialSize) {
  a->array = (int *)malloc(initialSize * sizeof(int));
  a->used = 0;
  a->size = initialSize;
}

void insertArray(Array *a, int element) {
  if (a->used == a->size)
  {
    a->size *= 2;
    a->array = (int *)realloc(a->array, a->size * sizeof(int));
  }
  a->array[a->used++] = element;
}

void freeArray(Array *a) {
  free(a->array);
  a->array = NULL;
  a->used = a->size = 0;
}

void deletebyindex (Array *a, int index)
{
	int temp;
	if (index > a->used - 1)
	{
		return;
	}
	else
	{
		for (int i = index;i<a->used-1;i++)
		{
			a->array[i] = a->array[i+1];
		}
		a->array[a->used - 1] = NULL;
		a->used -= 1;
		if(a->used < a->size/2)
		{
			a->size /= 2;
			a->array = (int *)realloc(a->array,a->size * sizeof(int));
		}
	}
}

struct Node
{
    Array data;
    Array data2;
    char key[20];
    struct Node *left;
    struct Node *right;
    int height;
};

int height(struct Node *N)
{
    if (N == NULL)
        return 0;
    return N->height;
}

int max(int a, int b)
{
    return (a > b)? a : b;
}

struct Node* newNode(char key[],int k, int j)
{
    struct Node* node = (struct Node*)
                        malloc(sizeof(struct Node));
    strcpy(node->key,key);
    node->left   = NULL;
    node->right  = NULL;
    node->height = 1;
    initArray(&(node->data),10);
    insertArray(&(node->data),k);
    initArray(&(node->data2),10);
    insertArray(&(node->data2),j);
    return(node);
}

void updateNode(struct Node *node, int k, int j)
{
  insertArray(&(node->data),k);
  insertArray(&(node->data2),j);
  return;
}

struct Node *rightRotate(struct Node *y)
{
    struct Node *x = y->left;
    struct Node *T2 = x->right;

    x->right = y;
    y->left = T2;

    y->height = max(height(y->left), height(y->right))+1;
    x->height = max(height(x->left), height(x->right))+1;

    return x;
}

struct Node *leftRotate(struct Node *x)
{
    struct Node *y = x->right;
    struct Node *T2 = y->left;

    y->left = x;
    x->right = T2;

    x->height = max(height(x->left), height(x->right))+1;
    y->height = max(height(y->left), height(y->right))+1;

    return y;
}

int getBalance(struct Node *N)
{
    if (N == NULL)
        return 0;
    return height(N->left) - height(N->right);
}

struct Node* insert(struct Node* node,char key[], int k, int j)
{
    if (node == NULL)
        return(newNode(key,k,j));

    if (strcmp(key,node->key)<0)
        node->left  = insert(node->left,key,k,j);
    else if (strcmp(key,node->key)>0)
        node->right = insert(node->right,key,k,j);
    else
    {
        updateNode(node,k,j);
        return node;
  	}

    node->height = 1 + max(height(node->left),height(node->right));

    int balance = getBalance(node);

    if (balance > 1 && strcmp(key,node->left->key)<0)
        return rightRotate(node);

    if (balance < -1 && strcmp(key,node->right->key)>0)
        return leftRotate(node);

    if (balance > 1 && strcmp(key,node->left->key)>0)
    {
        node->left =  leftRotate(node->left);
        return rightRotate(node);
    }

    if (balance < -1 && strcmp(key,node->right->key)<0)
    {
        node->right = rightRotate(node->right);
        return leftRotate(node);
    }

    return node;
}

struct Node * minValueNode(struct Node* node)
{
    struct Node* current = node;
    while (current->left != NULL)
        current = current->left;

    return current;
}

struct Node* delete(struct Node* root, char key[])
{

    if (root == NULL)
        return root;

    if ( strcmp(key,root->key)<0 )
        root->left = delete(root->left,key);

    else if( strcmp(key,root->key)>0 )
        root->right = delete(root->right, key);

    else
    {
        if( (root->left == NULL) || (root->right == NULL) )
        {
            struct Node *temp = root->left ? root->left : root->right;

            if (temp == NULL)
            {
                temp = root;
                root = NULL;
            }
            else
             *root = *temp;
            free(temp);
        }
        else
        {
            struct Node* temp = minValueNode(root->right);
            freeArray(&(root->data));
            root->data = temp->data;
            strcpy(root->key,temp->key);
            root->right = delete(root->right, temp->key);
        }
    }

    if (root == NULL)
      return root;

    root->height = 1 + max(height(root->left),
                           height(root->right));
    int balance = getBalance(root);

    if (balance > 1 && getBalance(root->left) >= 0)
        return rightRotate(root);

    if (balance > 1 && getBalance(root->left) < 0)
    {
        root->left =  leftRotate(root->left);
        return rightRotate(root);
    }

    if (balance < -1 && getBalance(root->right) <= 0)
        return leftRotate(root);

    if (balance < -1 && getBalance(root->right) > 0)
    {
        root->right = rightRotate(root->right);
        return leftRotate(root);
    }

    return root;
}

struct Node* Search(struct Node *node,char key[])
{
    if(node==NULL)
        return NULL;
    if(strcmp(key,node->key)==0)
        return node;
    else if (strcmp(key,node->key)<0)
        return Search(node->left,key);
    else if (strcmp(key,node->key)>0)
        return Search(node->right,key);
}

void inorder (struct Node *root)
{
	if(root!=NULL)
	{
		inorder(root->left);
		printf("%-15s %d",root->key,root->data.used);
		for(int i=0;i<root->data.used;i++)
        {
            printf(", %d(%d)",root->data.array[i],root->data2.array[i]);
        }
        printf("\n");
		inorder(root->right);
	}
}

int checkStopw(char* word, char* list[], int n)
{
    for(int j=0;j<n;j++)
    {
        if(strcmp(word,list[j])==0)
            return 0;
    }
    return 1;
}

int main()
{
    struct Node *root=NULL;
    char *list[100];
    char temp[30];
    int listLen=0;
    int firstWord;
    int validWord;
    FILE *file;
    char word[30];
    char c;
    int dong=1;
    int cot = 1;
    int i = 0;

    file = fopen("stopw.txt","r");
    while(!feof(file))
    {
        fscanf(file,"%s",temp);
        list[listLen]=malloc(strlen(temp)+1);
        strcpy(list[listLen],temp);
        listLen+=1;
    }
    fclose(file);

    file = fopen("vanban.txt","r");
    firstWord = 1;
    validWord = 1;
    while(!feof(file))
    {
        c = fgetc(file);
        if(c == ' ' || c == '\n' || c == '.' || c == ',' || c == '(' || c ==')'|| c =='!'|| c ==';'|| c =='\t'|| c =='?')
        {
            if(i>0)
            {
                word[i] = '\0';


                if(checkStopw(word,list,listLen)==1&&validWord==1)
                    root = insert(root,word,dong,cot);
                i = 0;
                cot+=1;
                validWord=1;
                firstWord=0;
                if(c=='.'||c=='?'||c=='!')
                {
                    firstWord = 1;
                }
            }
            if(c == '\n')
            {
                dong +=1;
                cot = 1;
            }
        }
        else
        {
            if((c<'a'||c>'z')&&(c<'A'||c>'Z'))
                validWord = 0;
            else if(firstWord!=1&&i==0&&'A'<=c&&c<='Z')
                validWord = 0;
            c = tolower(c);
            word[i] = c;
            i++;
        }
    }
    fclose(file);
    inorder(root);
    return 0;
}
